from zope import component
from Products.Five import BrowserView
from Products.CMFCore.utils import getToolByName
from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings


class GuidedSearchView(BrowserView):
    def deputies_url(self):
        self.settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        self.portal = getToolByName(self.context, 'portal_url').getPortalObject()
        return self.deputies_folder.absolute_url()

    def get_folder_at_path(self, path):
        current = self.portal
        for part in path.split('/'):
            if not part:
                continue
            current = getattr(current, part)
        return current

    _deputies_folder = None
    def deputies_folder(self):
        if self._deputies_folder:
            return self._deputies_folder
        path = self.settings.deputiesPath
        self._deputies_folder = self.get_folder_at_path(path)
        return self._deputies_folder
    deputies_folder = property(deputies_folder)

