from zope import interface, schema
from z3c.form import field, button
from plone.z3cform.layout import wrap_form
from Products.CMFCore.utils import getToolByName

from z3c.form.ptcompat import ViewPageTemplateFile

from z3c.relationfield.schema import RelationChoice, RelationList
from plone.formwidget.contenttree import ObjPathSourceBinder

from tabellio.searchform.interfaces import MessageFactory as _

from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

import form
import cmpfunctions

class SimpleSearchView(BrowserView):
    batch_macros = ViewPageTemplateFile('batch_macros.pt')
    js_macros = ViewPageTemplateFile('js_macros.pt')

    def deputy_search_form(self):
        f = form.DeputySearchForm(self.context, self.request)
        f.update()
        return f.render()

    def document_pfb_search_form(self):
        f = form.DocumentPfbSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def document_search_form(self):
        f = form.DocumentSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def dossier_search_form(self):
        f = form.DossierSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def question_search_form(self):
        f = form.QuestionSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def event_search_form(self):
        f = form.EventSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def deputy_form_url(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        try:
            return catalog(
                portal_type='Folder',
                object_provides=('tabellio.webviews.deputy.IDeputiesAndOthersFolderView',
                                 'tabellio.webviews.deputy.IPfbDeputiesFolderView',
                                 'tabellio.webviews.deputy.IFolderView'),
                limit=1)[0].getObject().absolute_url()
        except IndexError:
            return '#'

    def docdos_form_url(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        try:
            return catalog(
                portal_type='Folder',
                object_provides=('tabellio.searchform.form.IFolderWithPfbDocuments',
                                 'tabellio.searchform.form.IFolderWithDocuments'),
                limit=1)[0].getObject().absolute_url()
        except IndexError:
            return '#'

    def portal_url(self):
        return getToolByName(self.context, 'portal_url').getPortalPath()

    def event_form_url(self):
        return self.portal_url() + '/eventsearch'

    def deputy_results(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        if not self.request.form.get('SearchableText'):
            return []
        sorton = self.request.form.get('deputies-sort')
        c = catalog(
                portal_type=['themis.datatypes.deputy'],
                SearchableText=self.request.form.get('SearchableText'),
                sort_on="sortable_title", sort_order='ascending')
        if sorton == 'polgroup':
            def cmp_by_polgroup(x, y):
                xo = x.getObject()
                yo = y.getObject()
                if (xo.polgroup is None or xo.polgroup.to_object is None) and (
                        yo.polgroup is None or yo.polgroup.to_object is None):
                    return 0
                if (xo.polgroup is None or xo.polgroup.to_object is None):
                    return -1
                if (yo.polgroup is None or yo.polgroup.to_object is None):
                    return 1
                return cmp(xo.polgroup.to_object.id, yo.polgroup.to_object.id)
            c = list(c)
            c.sort(cmp_by_polgroup)
            return c
        else:
           return c

    def page_results(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        if not self.request.form.get('SearchableText'):
            return []
        return catalog(
                portal_type=['Document', 'File', 'Folder', 'Link'],
                SearchableText=self.request.form.get('SearchableText'),
                sort_on='created', sort_order='descending')

    def doc_results(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        if not self.request.form.get('SearchableText'):
            return []
        sorton = self.request.form.get('docs-sort', 'session')
        search_query = self.request.form.get('SearchableText')
        try:
            int(search_query)
        except ValueError:
            c = catalog(
                portal_type=['tabellio.documents.dossier',
                             'tabellio.documents.document',
                             'tabellio.documents.question'],
                SearchableText=search_query)
        else:
            c = catalog(
                portal_type=['tabellio.documents.document',
                             'tabellio.documents.dossier'],
                no=search_query)
        if sorton == 'session':
            cmpf = cmpfunctions.Cmp().cmp_session
        elif sorton == 'type':
            cmpf = cmpfunctions.Cmp().cmp_multitype
        elif sorton == 'number':
            cmpf = cmpfunctions.Cmp().cmp_number
        return sorted(c, cmpf);

    def event_results(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        if not self.request.form.get('SearchableText'):
            return []
        return catalog(
                portal_type=['tabellio.agenda.parlevent', 'tabellio.agenda.event',
                             'tabellio.agenda.comevent'],
                SearchableText=self.request.form.get('SearchableText'),
                sort_on='start', sort_order='descending')

    def get_batchlinkparams(self):
        d = dict()
        for key in self.request.form:
            d[key] = self.request.form[key]
            if type(d[key]) is str:
                d[key] = unicode(d[key], 'utf-8').encode('utf-8')
            elif type(d[key]) is unicode:
                d[key] = d[key].encode('utf-8')
        return d
