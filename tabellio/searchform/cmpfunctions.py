import re
import Missing
from tabellio.documents.typenames import MAPPING
from plone.i18n.normalizer.fr import normalizer
import tabellio.config.utils

def get_no_as_int(i):
    if i:
        try:
            return int(i.split('-')[0])
        except ValueError:
            pass
        try:
            return re.findall('\d+', i)[0]
        except IndexError:
            pass
        return 99999
    return 0

class Cmp:

    def cmp_doctype(self, x, y):
        if not (x.doctype is None or y.doctype is None or
                x.doctype is Missing.Value or y.doctype is Missing.Value):
            type_x = normalizer.normalize(MAPPING.get(x.doctype, x.doctype))
            type_y = normalizer.normalize(MAPPING.get(y.doctype, y.doctype))
            t = cmp(type_x, type_y)
        else:
            t = -cmp(x.doctype, y.doctype)
        if t == 0:
            t = -cmp(x.session, y.session)
            if t == 0:
                t = -cmp(x.no, y.no)
                if t == 0:
                    t = cmp(x.nodoc, y.nodoc)
        return t

    def cmp_multitype(self, x, y):
        type_x = 'ZZZZ'
        type_y = 'ZZZZ'
        for attr in ('doctype', 'dostype', 'questype'):
            if hasattr(x, attr) and getattr(x, attr) not in (None, Missing.Value):
                dtype = getattr(x, attr)
                type_x = normalizer.normalize(MAPPING.get(dtype, dtype))
            if hasattr(y, attr) and getattr(y, attr) not in (None, Missing.Value):
                dtype = getattr(y, attr)
                type_y = normalizer.normalize(MAPPING.get(dtype, dtype))
        t = cmp(type_x, type_y)
        if t == 0:
            t = -cmp(x.session, y.session)
            if t == 0:
                t = -cmp(x.no, y.no)
                if t == 0:
                    t = cmp(x.nodoc, y.nodoc)
        return t

    _ordered_sessions = None
    def ordered_sessions(self):
        if self._ordered_sessions: return self._ordered_sessions
        self._ordered_sessions = tabellio.config.utils.get_ordered_sessions()
        return self._ordered_sessions
    ordered_sessions = property(ordered_sessions)

    def cmp_session_name(self, x, y):
        if x is y or x == y:
            return 0
        if x not in self.ordered_sessions:
            return -1
        if y not in self.ordered_sessions:
            return +1
        return cmp(self.ordered_sessions.index(x), self.ordered_sessions.index(y))

    def cmp_dostype(self, x, y):
        if not (x.dostype is None or y.dostype is None or
                x.dostype is Missing.Value or y.dostype is Missing.Value):
            type_x = normalizer.normalize(MAPPING.get(x.dostype, x.dostype))
            type_y = normalizer.normalize(MAPPING.get(y.dostype, y.dostype))
        else:
            t = -cmp(x.dostype, y.dostype)
        t = cmp(type_x, type_y)
        if t == 0:
            t = -self.cmp_session_name(x.session, y.session)
            if t == 0:
                t = -cmp(x.no, y.no)
        return t

    def cmp_questype(self, x, y):
        if not (x.questype is None or y.questype is None or
                x.questype is Missing.Value or y.questype is Missing.Value):
            type_x = normalizer.normalize(MAPPING.get(x.questype, x.questype))
            type_y = normalizer.normalize(MAPPING.get(y.questype, y.questype))
        else:
            t = -cmp(x.questype, y.questype)
        t = cmp(type_x, type_y)
        if t == 0:
            t = -self.cmp_session_name(x.session, y.session)
            if t == 0:
                t = -cmp(x.dateDoc, y.dateDoc)
        return t

    def cmp_number(self, x, y):
        t = -cmp(get_no_as_int(x.no), get_no_as_int(y.no))
        if t == 0:
            t = -self.cmp_session_name(x.session, y.session)
            if t == 0:
                t = cmp(x.nodoc, y.nodoc)
        return t

    def cmp_session(self, x, y):
        t = -self.cmp_session_name(x.session, y.session)
        if t == 0:
            t = -cmp(get_no_as_int(x.no), get_no_as_int(y.no))
            if t == 0 and hasattr(x, 'nodoc'):
                t = cmp(x.nodoc, y.nodoc)
            elif t == 0 and hasattr(x, 'dateDoc'):
                t = cmp(x.dateDoc, y.dateDoc)
        return t

