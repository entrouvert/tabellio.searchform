# -*- coding: utf-8 -*-

import re
import time
import datetime
import logging

from five import grok
from plone.memoize import instance, ram
from zope import interface, schema, component
from z3c.form import form, field, button
from plone.z3cform.layout import wrap_form
from Products.CMFCore.utils import getToolByName
from zope.interface import implements

from z3c.form.ptcompat import ViewPageTemplateFile
import z3c.form.interfaces
from z3c.form.browser import text
from z3c.form import widget
from z3c.form.interfaces import ITextWidget, ICheckBoxWidget
from z3c.form.browser.checkbox import CheckBoxWidget
from z3c.form.browser.radio import RadioWidget

from z3c.relationfield.schema import RelationChoice, RelationList
from plone.formwidget.contenttree import ObjPathSourceBinder
import plone.directives.form

from tabellio.searchform.interfaces import MessageFactory as _

import Missing

from Products.Five import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from plone.registry.interfaces import IRegistry

from tabellio.config.interfaces import ITabellioSettings
import tabellio.config.utils
from tabellio.documents.typenames import MAPPING

import cmpfunctions

log = logging.getLogger('Plone')

class ListAuthorsView(BrowserView):
    def get_folder_at_path(self, path):
        current = self.portal
        for part in path.split('/'):
            if not part:
                continue
            current = getattr(current, part)
        return current

    _deputies_folder = None
    def deputies_folder(self):
        if self._deputies_folder:
            return self._deputies_folder
        path = self.settings.deputiesPath
        self._deputies_folder = self.get_folder_at_path(path)
        return self._deputies_folder
    deputies_folder = property(deputies_folder)

    def __call__(self):
        from plone.i18n.normalizer.fr import normalizer

        self.portal = getToolByName(self.context, 'portal_url').getPortalObject()
        self.settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        self.request.response.setHeader('Content-type', 'text/plain')

        id = self.request.form.get('id')
        if id:
            try:
                t = getattr(self.deputies_folder, id)
            except AttributeError:
                return 'XXX'
            return t.Title()

        s = []
        q = normalizer.normalize(unicode(self.request.form.get('q'), 'utf-8')).lower()
        for object in self.deputies_folder.objectValues():
            if object.portal_type != 'themis.datatypes.deputy':
                continue
            for name_part in re.split(r'[-\s]', normalizer.normalize(object.Title()).lower()):
                if name_part.startswith(q):
                    s.append(object)
                    break
        def cmp_deputy(x, y):
            return cmp(normalizer.normalize(x.Title()).lower(),
                       normalizer.normalize(y.Title()).lower())
        s.sort(cmp_deputy)
        return '\n'.join(['%s|%s' % (x.Title(), x.id) for x in s])

class ListPolgroupsView(BrowserView):
    def get_folder_at_path(self, path):
        current = self.portal
        for part in path.split('/'):
            if not part:
                continue
            current = getattr(current, part)
        return current

    _polgroups_folder = None
    def polgroups_folder(self):
        if self._polgroups_folder:
            return self._polgroups_folder
        path = self.settings.polgroupsPath
        self._polgroups_folder = self.get_folder_at_path(path)
        return self._polgroups_folder
    polgroups_folder = property(polgroups_folder)

    def __call__(self):
        self.portal = getToolByName(self.context, 'portal_url').getPortalObject()
        self.settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        self.request.response.setHeader('Content-type', 'text/plain')

        id = self.request.form.get('id')
        if id:
            try:
                t = getattr(self.polgroups_folder, id)
            except AttributeError:
                return 'XXX'
            return t.Title()

        s = []
        q = unicode(self.request.form.get('q'), 'utf-8').lower()
        for object in self.polgroups_folder.objectValues():
            if object.portal_type != 'themis.datatypes.polgroup':
                continue
            if q in object.id or q in object.Title().lower():
                s.append(object)
        return '\n'.join(['%s|%s' % (x.Title(), x.id) for x in s])


class ListTopicsView(BrowserView):
    def __call__(self):
        from plone.i18n.normalizer.fr import normalizer
        topics = tabellio.config.utils.get_topics_dict()
        self.request.response.setHeader('Content-type', 'text/plain')

        id = self.request.form.get('id')
        if id:
            try:
                return '%s' % topics.get(id, id)[0]
            except KeyError:
                return 'XXX'

        query_terms = [normalizer.normalize(x).lower() for x in unicode(self.request.form.get('q'), 'utf-8').split()]
        print 'query terms:', query_terms

        sorted_topics = topics.items()
        sorted_topics.sort(lambda x,y: cmp(x[1][1].lower(), y[1][1].lower()))

        r = []
        for key, value in sorted_topics:
            for term in query_terms:
                if not term in value[1].lower():
                    break
            else:
                r.append('%s|%s' % (value[0], key))
                if len(r) > 30:
                    break
        return '\n'.join(r)


class IAuthorsWidget(ITextWidget):
    pass

class AuthorsWidget(text.TextWidget):
    implements(IAuthorsWidget)
    klass = u'authors'

def FieldAuthorsWidget(field, request):
    return widget.FieldWidget(field, AuthorsWidget(request))

class IPolgroupsWidget(ITextWidget):
    pass

class PolgroupsWidget(text.TextWidget):
    implements(IPolgroupsWidget)
    klass = u'polgroups'

def FieldPolgroupsWidget(field, request):
    return widget.FieldWidget(field, PolgroupsWidget(request))

class ITopicsWidget(ITextWidget):
    pass

class IDroppableCheckBoxWidget(ICheckBoxWidget):
    pass

class TopicsWidget(text.TextWidget):
    implements(ITopicsWidget)
    klass = u'topics'

class DroppableCheckBoxWidget(CheckBoxWidget):
    implements(IDroppableCheckBoxWidget)
    input_template = ViewPageTemplateFile('droppable-checkbox-widget-input.pt')

    def render(self):
        if self.mode == z3c.form.interfaces.INPUT_MODE:
            return self.input_template(self)
        return CheckBoxWidget.render(self)

def FieldTopicsWidget(field, request):
    return widget.FieldWidget(field, TopicsWidget(request))

def FieldDroppedCheckboxWidget(field, request):
    return widget.FieldWidget(field, DroppableCheckBoxWidget(request))

def FieldRadioboxesWidget(field, request):
    return widget.FieldWidget(field, RadioWidget(request))

class IFolderWithDocuments(interface.Interface):
    pass

class IFolderWithPfbDocuments(interface.Interface):
    pass

def cmp_term(x, y):
   from plone.i18n.normalizer.fr import normalizer
   return cmp(normalizer.normalize(x.title), normalizer.normalize(y.title))

@grok.provider(IContextSourceBinder)
def possible_document_types(context):
    catalog = getToolByName(context, 'portal_catalog')
    possible_doctypes = catalog.uniqueValuesFor('doctype')
    terms = []
    for doctype in possible_doctypes:
        if not doctype:
            continue
        if doctype == u'Réponse à la question écrite':
            continue
        doctype_id = doctype.encode('ascii', 'replace')
        doctype_str = MAPPING.get(doctype, doctype)
        terms.append(SimpleVocabulary.createTerm(doctype_id, doctype_id, doctype_str))
    terms.sort(cmp_term)
    return SimpleVocabulary(terms)

def get_docdos_type_id(context, type_id):
    if type_id in MAPPING.keys():
        return type_id
    catalog = getToolByName(context, 'portal_catalog')
    possible_doctypes = catalog.uniqueValuesFor('doctype')
    possible_dostypes = catalog.uniqueValuesFor('dostype')
    possible_questypes = catalog.uniqueValuesFor('questype')
    for iter_type_id in (possible_dostypes + possible_doctypes + possible_questypes):
        if iter_type_id is None:
            continue
        rep_id = iter_type_id.encode('ascii', 'replace')
        if type_id in (iter_type_id, rep_id):
            return iter_type_id
    return None


@grok.provider(IContextSourceBinder)
def possible_dossier_types(context):
    catalog = getToolByName(context, 'portal_catalog')
    possible_dostypes = catalog.uniqueValuesFor('dostype')
    terms = []
    for dostype in possible_dostypes:
        if not dostype:
            continue
        dostype_id = dostype.encode('ascii', 'replace')
        dostype_str = MAPPING.get(dostype, dostype)
        terms.append(SimpleVocabulary.createTerm(dostype_id, dostype_id, dostype_str))
    terms.sort(cmp_term)
    return SimpleVocabulary(terms)

@grok.provider(IContextSourceBinder)
def possible_question_types(context):
    catalog = getToolByName(context, 'portal_catalog')
    possible_questypes = catalog.uniqueValuesFor('questype')
    terms = []
    for questype in possible_questypes:
        if not questype:
            continue
        questype_id = questype.encode('ascii', 'replace')
        questype_str = MAPPING.get(questype, questype)
        terms.append(SimpleVocabulary.createTerm(questype_id, questype_id, questype_str))
    terms.sort(cmp_term)
    return SimpleVocabulary(terms)

@grok.provider(IContextSourceBinder)
def possible_sessions(context):
    terms = []
    for term in tabellio.config.utils.get_legisl_and_sessions():
        term_id = term.encode('ascii', 'replace')
        terms.append(SimpleVocabulary.createTerm(term_id, term_id, term))
    return SimpleVocabulary(terms)

@grok.provider(IContextSourceBinder)
def possible_polgroups(context):
    current = getToolByName(context, 'portal_url').getPortalObject()
    settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
    if not settings.polgroupsPath:
        return SimpleVocabulary([])
    for part in settings.polgroupsPath.split('/'):
        if not part:
             continue
        current = getattr(current, part)

    active_groups = []
    inactive_groups = []
    for object in current.objectValues():
        if object.portal_type != 'themis.datatypes.polgroup':
            continue
        polgroup_id = object.id
        polgroup_str = object.Title()
        if object.active:
            active_groups.append(SimpleVocabulary.createTerm(polgroup_id, polgroup_id, polgroup_str))
        else:
            inactive_groups.append(SimpleVocabulary.createTerm(polgroup_id, polgroup_id, polgroup_str))
    # keep active groups ordered by their position
    inactive_groups.sort(lambda x,y: cmp(x.value, y.value))
    return SimpleVocabulary(active_groups + inactive_groups)

@grok.provider(IContextSourceBinder)
def possible_active_polgroups(context):
    terms = []

    current = getToolByName(context, 'portal_url').getPortalObject()
    settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
    for part in settings.polgroupsPath.split('/'):
        if not part:
             continue
        current = getattr(current, part)

    for object in current.objectValues():
        if object.portal_type != 'themis.datatypes.polgroup':
            continue
        if not object.active:
            continue
        polgroup_id = object.id
        polgroup_str = object.Title()
        terms.append(SimpleVocabulary.createTerm(polgroup_id, polgroup_id, polgroup_str))
    return SimpleVocabulary(terms)



@grok.provider(IContextSourceBinder)
def possible_topics(context):
    catalog = getToolByName(context, 'portal_catalog')
    topics = tabellio.config.utils.get_topics_dict()
    terms = []
    for key, value in sorted(topics.items()):
        topic_id = key
        topic_str = value[0]
        terms.append(SimpleVocabulary.createTerm(topic_id, topic_id, topic_str))
    return SimpleVocabulary(terms)


@grok.provider(IContextSourceBinder)
def possible_sort_options(context):
    terms = []
    terms.append(SimpleVocabulary.createTerm('session', 'session', _('Session')))
    terms.append(SimpleVocabulary.createTerm('type', 'type', _('Type')))
    terms.append(SimpleVocabulary.createTerm('number', 'number', _('Number')))
    return SimpleVocabulary(terms)


@grok.provider(IContextSourceBinder)
def possible_sort_options_2(context):
    terms = []
    terms.append(SimpleVocabulary.createTerm('session', 'session', _('Session')))
    terms.append(SimpleVocabulary.createTerm('type', 'type', _('Type')))
    return SimpleVocabulary(terms)




class IDocumentSearch(interface.Interface):
    search_type_is_document = schema.TextLine(title=u'Search Type', default=u'1', required=False)
    nodoc = schema.TextLine(title=_(u'Dossier Number'), required=False)
    nosuite = schema.TextLine(title=_(u'Suite Number'), required=False)
    doctype = schema.Choice(title=_(u'Document Type'), required=False,
                source=possible_document_types)
    #l_doctypes = schema.List(title=_(u'Document Type'), required=False,
    #                value_type=schema.Choice(title=_(u'Type'), required=False,
    #                        source=possible_document_types))
    ttitle = schema.TextLine(title=_(u'Title'), required=False)
    text = schema.TextLine(title=_(u'Text'), required=False)
    authors = schema.TextLine(title=_(u'Authors'), required=False)
    polgroups = schema.Choice(title=_(u'Political Groups'), required=False, source=possible_polgroups)
#    l_polgroups = schema.List(title=_(u'Political Groups'), required=False,
#                    value_type=schema.Choice(title=_(u'Political Group'),
#                    required=False, source=possible_polgroups))
    topics = schema.TextLine(title=_(u'Topics'), required=False)
    session = schema.Choice(title=_(u'Legislature / Session'), required=False,
                source=possible_sessions)
    start = schema.Date(title=_(u'Document Start Date'), required=False)
    end = schema.Date(title=_(u'Document End Date'), required=False)

    sort_on = schema.Choice(title=_(u'Sort By'), required=True,
                default='session', source=possible_sort_options)


class DocumentSearchForm(form.Form):
    method = 'get'
    prefix = 'document'
    fields = field.Fields(IDocumentSearch)
    fields['authors'].widgetFactory = FieldAuthorsWidget
    fields['topics'].widgetFactory = FieldTopicsWidget
    #fields['l_doctypes'].widgetFactory = FieldDroppedCheckboxWidget
    #fields['l_polgroups'].widgetFactory = FieldDroppedCheckboxWidget
    ignoreContext = True
    template = ViewPageTemplateFile('form_templates/view_form.pt')

    def updateWidgets(self):
        super(DocumentSearchForm, self).updateWidgets()
        self.widgets['search_type_is_document'].mode = z3c.form.interfaces.HIDDEN_MODE

    @button.buttonAndHandler(_(u'Search'))
    def handleApply(self, action):
        pass


class IDossierSearch(interface.Interface):
    search_type_is_dossier = schema.TextLine(title=u'Search Type', default=u'1', required=False)

    nodos = schema.TextLine(title=_(u'Dossier Number'), required=False)
    dostype = schema.Choice(title=_(u'Dossier Type'), required=False,
                source=possible_dossier_types)
    #l_dostypes = schema.List(title=_(u'Dossier Type'), required=False,
    #                value_type=schema.Choice(title=_(u'Type'), required=False,
    #                        source=possible_dossier_types))
    ttitle = schema.TextLine(title=_(u'Title'), required=False)
    authors = schema.TextLine(title=_(u'Authors'), required=False)
    polgroups = schema.Choice(title=_(u'Political Groups'), required=False, source=possible_polgroups)
    #l_polgroups = schema.List(title=_(u'Political Groups'), required=False,
    #                value_type=schema.Choice(title=_(u'Political Group'),
    #                required=False, source=possible_polgroups))
    participants = schema.TextLine(title=_(u'Participants'), required=False)
    topics = schema.TextLine(title=_(u'Topics'), required=False)
    session = schema.Choice(title=_(u'Legislature / Session'), required=False,
                source=possible_sessions)
    adopted = schema.Bool(title=_(u'Limit to adopted'), required=False, default=False)
    decreted = schema.Bool(title=_(u'Limit to decreted'), required=False, default=False)
    start = schema.Date(title=_(u'Dossier Start Date'), required=False)
    end = schema.Date(title=_(u'Dossier End Date'), required=False)

    sort_on = schema.Choice(title=_(u'Sort By'), required=True,
                default='session', source=possible_sort_options)


class DossierSearchForm(form.Form):
    prefix = 'dossier'
    method = 'get'
    fields = field.Fields(IDossierSearch)
    fields['authors'].widgetFactory = FieldAuthorsWidget
    fields['participants'].widgetFactory = FieldAuthorsWidget
    #fields['l_dostypes'].widgetFactory = FieldDroppedCheckboxWidget
    #fields['l_polgroups'].widgetFactory = FieldDroppedCheckboxWidget
    fields['topics'].widgetFactory = FieldTopicsWidget
    ignoreContext = True
    template = ViewPageTemplateFile('form_templates/view_form.pt')

    def updateWidgets(self):
        super(DossierSearchForm, self).updateWidgets()
        self.widgets['search_type_is_dossier'].mode = z3c.form.interfaces.HIDDEN_MODE

    @button.buttonAndHandler(_(u'Search'))
    def handleApply(self, action):
        pass


class IQuestionSearch(interface.Interface):
    search_type_is_question = schema.TextLine(title=u'Search Type', default=u'1', required=False)

    #l_questypes = schema.List(title=_(u'Question Type'), required=False,
    #                value_type=schema.Choice(title=_(u'Type'), required=False,
    #                        source=possible_question_types))
    questype = schema.Choice(title=_(u'Question Type'), required=False,
                source=possible_question_types)
    ttitle = schema.TextLine(title=_(u'Title'), required=False)
    authors = schema.TextLine(title=_(u'Authors'), required=False)
    polgroups = schema.Choice(title=_(u'Political Groups'), required=False, source=possible_polgroups)
    #l_polgroups = schema.List(title=_(u'Political Groups'), required=False,
    #                value_type=schema.Choice(title=_(u'Political Group'),
    #                required=False, source=possible_polgroups))
    topics = schema.TextLine(title=_(u'Topics'), required=False)
    session = schema.Choice(title=_(u'Legislature / Session'), required=False,
                source=possible_sessions)
    start = schema.Date(title=_(u'Question Start Date'), required=False)
    end = schema.Date(title=_(u'Question End Date'), required=False)

    sort_on = schema.Choice(title=_(u'Sort By'), required=True,
                default='session', source=possible_sort_options_2)


class QuestionSearchForm(form.Form):
    prefix = 'question'
    fields = field.Fields(IQuestionSearch)
    fields['authors'].widgetFactory = FieldAuthorsWidget
    #fields['l_polgroups'].widgetFactory = FieldDroppedCheckboxWidget
    #fields['l_questypes'].widgetFactory = FieldDroppedCheckboxWidget
    fields['topics'].widgetFactory = FieldTopicsWidget
    ignoreContext = True
    template = ViewPageTemplateFile('form_templates/view_form.pt')

    def updateWidgets(self):
        super(QuestionSearchForm, self).updateWidgets()
        self.widgets['search_type_is_question'].mode = z3c.form.interfaces.HIDDEN_MODE

    @button.buttonAndHandler(_(u'Search'))
    def handleApply(self, action):
        pass


class IQuestionPfbSearch(interface.Interface):
    search_type_is_question = schema.TextLine(title=u'Search Type', default=u'1', required=False)

    l_questypes = schema.List(title=_(u'Question Type'), required=False,
                    value_type=schema.Choice(title=_(u'Type'), required=False,
                            source=possible_question_types))
    authors = schema.TextLine(title=_(u'Authors'), required=False)
    session = schema.Choice(title=_(u'Legislature / Session'), required=False,
                source=possible_sessions)
    ttitle = schema.TextLine(title=_(u'Title'), required=False)
    l_topics = schema.List(title=_(u'Topics'), required=False,
                    value_type=schema.Choice(required=False,
                    source=possible_topics));
    start = schema.Date(title=_(u'Question Start Date'), required=False)
    end = schema.Date(title=_(u'Question End Date'), required=False)

    sort_on = schema.Choice(title=_(u'Sort By'), required=True,
                default='session', source=possible_sort_options_2)

class QuestionPfbSearchForm(form.Form):
    prefix = 'question'
    ignoreContext = True
    template = ViewPageTemplateFile('form_templates/view_form.pt')
    fields = field.Fields(IQuestionPfbSearch)
    fields['authors'].title = _(u'Author(s)')
    fields['authors'].widgetFactory = FieldAuthorsWidget
    fields['l_questypes'].widgetFactory = FieldDroppedCheckboxWidget
    fields['l_topics'].widgetFactory = FieldDroppedCheckboxWidget

    def updateWidgets(self):
        super(QuestionPfbSearchForm, self).updateWidgets()
        self.widgets['search_type_is_question'].mode = z3c.form.interfaces.HIDDEN_MODE

    @button.buttonAndHandler(_(u'Submit'))
    def handleApply(self, action):
        pass

class IDocumentPfbSearch(interface.Interface):
    search_type_is_document = schema.TextLine(title=u'Search Type', default=u'1')

    l_doctypes = schema.List(title=_(u'Type'), required=False,
                    value_type=schema.Choice(title=_(u'Type'), required=False,
                            source=possible_document_types))
    authors = schema.TextLine(title=_(u'Author(s)'), required=False)
    #l_polgroups = schema.List(title=_(u'Political Groups'), required=False,
    #                value_type=schema.Choice(title=_(u'Political Group'),
    #                required=False, source=possible_polgroups))
    session = schema.Choice(title=_(u'Legislature / Session'), required=False,
                source=possible_sessions)
    ttitle = schema.TextLine(title=_(u'Title'), required=False)
    l_topics = schema.List(title=_(u'Topics'), required=False,
                    value_type=schema.Choice(required=False,
                    source=possible_topics));
    text = schema.TextLine(title=_(u'Text'), required=False)
    nodoc = schema.TextLine(title=_(u'Document Number'), required=False)
    start = schema.Date(title=_(u'Document Start Date'), required=False)
    end = schema.Date(title=_(u'Document End Date'), required=False)

    sort_on = schema.Choice(title=_(u'Sort By'), required=True,
                default='session', source=possible_sort_options)


class DocumentPfbSearchForm(form.Form):
    method = 'get'
    prefix = 'document'
    fields = field.Fields(IDocumentPfbSearch)
    fields['authors'].widgetFactory = FieldAuthorsWidget
    fields['l_doctypes'].widgetFactory = FieldDroppedCheckboxWidget
    #fields['l_polgroups'].widgetFactory = FieldDroppedCheckboxWidget
    fields['l_topics'].widgetFactory = FieldDroppedCheckboxWidget
    ignoreContext = True
    template = ViewPageTemplateFile('form_templates/view_form.pt')

    def updateWidgets(self):
        super(DocumentPfbSearchForm, self).updateWidgets()
        self.widgets['search_type_is_document'].mode = z3c.form.interfaces.HIDDEN_MODE

    @button.buttonAndHandler(_(u'Submit'))
    def handleApply(self, action):
        pass


class IAdoptedDocumentPfbSearch(interface.Interface):
    search_type_is_adopteddocument = schema.TextLine(title=u'Search Type', default=u'1')

    l_doctypes = schema.List(title=_(u'Type'), required=False,
                    value_type=schema.Choice(title=_(u'Type'), required=False,
                            values=[u'Projet', u'Proposition']))
    authors = schema.TextLine(title=_(u'Author(s)'), required=False)
    #l_polgroups = schema.List(title=_(u'Political Groups'), required=False,
    #                value_type=schema.Choice(title=_(u'Political Group'),
    #                required=False, source=possible_polgroups))
    session = schema.Choice(title=_(u'Legislature / Session'), required=False,
                source=possible_sessions)
    ttitle = schema.TextLine(title=_(u'Title'), required=False)
    l_topics = schema.List(title=_(u'Topics'), required=False,
                    value_type=schema.Choice(required=False,
                    source=possible_topics));
    text = schema.TextLine(title=_(u'Text'), required=False)
    nodoc = schema.TextLine(title=_(u'Document Number'), required=False)
    start = schema.Date(title=_(u'Document Start Date'), required=False)
    end = schema.Date(title=_(u'Document End Date'), required=False)
    adopted = schema.Bool(title=_(u'Limit to adopted'), required=False, default=True)

    sort_on = schema.Choice(title=_(u'Sort By'), required=True,
                default='session', source=possible_sort_options)


class AdoptedDocumentPfbSearchForm(form.Form):
    prefix = 'adopteddocument'
    fields = field.Fields(IAdoptedDocumentPfbSearch)
    fields['authors'].widgetFactory = FieldAuthorsWidget
    fields['l_doctypes'].widgetFactory = FieldDroppedCheckboxWidget
    #fields['l_polgroups'].widgetFactory = FieldDroppedCheckboxWidget
    fields['l_topics'].widgetFactory = FieldDroppedCheckboxWidget
    ignoreContext = True
    template = ViewPageTemplateFile('form_templates/view_form.pt')

    def updateWidgets(self):
        super(AdoptedDocumentPfbSearchForm, self).updateWidgets()
        self.widgets['adopted'].mode = z3c.form.interfaces.HIDDEN_MODE
        self.widgets['search_type_is_adopteddocument'].mode = z3c.form.interfaces.HIDDEN_MODE

    @button.buttonAndHandler(_(u'Submit'))
    def handleApply(self, action):
        pass



class IDeputySearch(interface.Interface):
    search_type_is_deputy = schema.TextLine(title=u'Search Type', default=u'1', required=False)

    name = schema.TextLine(title=_(u'Last Name / First Name'), required=False)
    polgroup = schema.Choice(title=_(u'Political Group'),
                    required=False, source=possible_active_polgroups)


class DeputySearchForm(form.Form):
    prefix = 'deputy'
    fields = field.Fields(IDeputySearch)
    ignoreContext = True
    template = ViewPageTemplateFile('form_templates/view_form.pt')

    def updateWidgets(self):
        super(DeputySearchForm, self).updateWidgets()
        self.widgets['search_type_is_deputy'].mode = z3c.form.interfaces.HIDDEN_MODE

    @button.buttonAndHandler(_(u'Submit'))
    def handleApply(self, action):
        pass


class IEventSearch(interface.Interface):
    search_type_is_event = schema.TextLine(title=u'Search Type', default=u'1', required=False)

    text = schema.TextLine(title=_(u'Title / Location'), required=False)
    start = schema.Date(title=_(u'Start'), required=False)
    end = schema.Date(title=_(u'End'), required=False)


class EventSearchForm(form.Form):
    prefix = 'event'
    fields = field.Fields(IEventSearch)
    ignoreContext = True
    template = ViewPageTemplateFile('form_templates/view_form.pt')

    def updateWidgets(self):
        super(EventSearchForm, self).updateWidgets()
        self.widgets['search_type_is_event'].mode = z3c.form.interfaces.HIDDEN_MODE

    @button.buttonAndHandler(_(u'Submit'))
    def handleApply(self, action):
        pass


class IGlobalSearchForm(interface.Interface):
    search_type_is_document = schema.TextLine(title=u'Search Type', required=False)
    search_type_is_adopteddocument = schema.TextLine(title=u'Search Type', required=False)
    search_type_is_dossier = schema.TextLine(title=u'Search Type', required=False)
    search_type_is_question = schema.TextLine(title=u'Search Type', required=False)

    nodoc = schema.TextLine(title=_(u'Document Number'), required=False)
    nosuite = schema.TextLine(title=_(u'Suite Number'), required=False)
    doctype = schema.Choice(title=_(u'Type'), required=False,
                source=possible_document_types)

    nodos = schema.TextLine(title=_(u'Dossier Number'), required=False)
    dostype = schema.Choice(title=_(u'Type'), required=False,
                source=possible_dossier_types)

    questype = schema.Choice(title=_(u'Type'), required=False,
                source=possible_question_types)

    ttitle = schema.TextLine(title=_(u'Title'), required=False)
    text = schema.TextLine(title=_(u'Text'), required=False)
    authors = schema.TextLine(title=_(u'Authors'), required=False)
    participants = schema.TextLine(title=_(u'Participants'), required=False)
    #polgroups = schema.TextLine(title=_(u'Political Groups'), required=False)
    polgroups = schema.Choice(title=_(u'Political Groups'), required=False, source=possible_polgroups)
    topics = schema.TextLine(title=_(u'Topics'), required=False)
    session = schema.Choice(title=_(u'Legislature / Session'), required=False,
                source=possible_sessions)
    start = schema.Date(title=_(u'Start'), required=False)
    end = schema.Date(title=_(u'End'), required=False)
    adopted = schema.Bool(title=_(u'Limit to adopted'), required=False, default=False)
    decreted = schema.Bool(title=_(u'Limit to decreted'), required=False, default=False)

    l_doctypes = schema.List(title=_(u'Types'), required=False,
                    value_type=schema.Choice(title=_(u'Type'), required=False,
                            source=possible_document_types))
    l_dostypes = schema.List(title=_(u'Types'), required=False,
                    value_type=schema.Choice(title=_(u'Type'), required=False,
                            source=possible_dossier_types))
    l_questypes = schema.List(title=_(u'Types'), required=False,
                    value_type=schema.Choice(title=_(u'Type'), required=False,
                            source=possible_question_types))
    l_polgroups = schema.List(title=_(u'Political Groups'), required=False,
                    value_type=schema.Choice(title=_(u'Political Group'),
                    required=False, source=possible_polgroups))
    l_topics = schema.List(title=_(u'Topics'), required=False,
                    value_type=schema.Choice(required=False,
                    source=possible_topics));

    sort_on = schema.Choice(title=_(u'Sort By'), required=False,
                default='type', source=possible_sort_options)


class GlobalSearchForm(form.Form):
    fields = field.Fields(IGlobalSearchForm)
    ignoreContext = True

class SearchView(BrowserView):
    batch_macros = ViewPageTemplateFile('batch_macros.pt')
    js_macros = ViewPageTemplateFile('js_macros.pt')

    def portal_url(self):
        return getToolByName(self.context, 'portal_url').getPortalPath()

    def db_connection(self):
        portal = getToolByName(self.context, 'portal_url').getPortalObject()
        return portal.db._wrapper.connection
    db_connection = property(db_connection)

    def _get_ids_cache_key(method, self, text):
        return (text, )

    @ram.cache(_get_ids_cache_key)
    def get_ids_from_postgres(self, text):
        try:
            cursor = self.db_connection.cursor()
        except AttributeError:
            return []
        log.info('Calling to postgresql for search (%r)' % text)
        # looks like there's no way to quote things properly for to_tsquery,
        from plone.i18n.normalizer.fr import normalizer
        text = re.sub(r'[^\w\s]', ' ', normalizer.normalize(text))
        cursor.execute("""SELECT t_document.id
                            FROM t_document JOIN t_file
                              ON (t_document.text1id = t_file.fileid OR
                                  t_document.textdefid = t_file.fileid)
                           WHERE object_fts @@ to_tsquery('default_french', '''%s''')""" % text)
        ids = [x[0] for x in cursor.fetchall()]
        cursor.close()
        return ids

    def get_batchlinkparams(self):
        d = dict()
        for key in self.request.form:
            d[key] = self.request.form[key]
            if type(d[key]) is str:
                d[key] = unicode(d[key], 'utf-8').encode('utf-8')
            elif type(d[key]) is unicode:
                d[key] = d[key].encode('utf-8')
        return d

    def search_results(self, search_type):
        print 'search type:', search_type

        if self.request.form.get('document.widgets.search_type_is_document'):
            GlobalSearchForm.prefix = 'document'
        elif self.request.form.get('dossier.widgets.search_type_is_dossier'):
            GlobalSearchForm.prefix = 'dossier'
        elif self.request.form.get('question.widgets.search_type_is_question'):
            GlobalSearchForm.prefix = 'question'
        elif self.request.form.get('adopteddocument.widgets.search_type_is_adopteddocument'):
            GlobalSearchForm.prefix = 'adopteddocument'

        f = GlobalSearchForm(self.context, self.request)
        f.update()

        data, errors = f.extractData()
        kw = {}

        if not data.get('search_type_is_%s' % search_type):
            return None
        print 'data:', data

        if data.get('ttitle'):
            kw['Title'] = data.get('ttitle')

        if data.get('nodoc'):
            kw['no'] = data.get('nodoc')

        if data.get('nosuite'):
            kw['nodoc'] = data.get('nosuite')

        if data.get('nodos'):
            kw['no'] = data.get('nodos')

        if data.get('doctype'):
            kw['doctype'] = get_docdos_type_id(self.context, data.get('doctype'))
        if data.get('dostype'):
            kw['dostype'] = get_docdos_type_id(self.context, data.get('dostype'))
        if data.get('questype'):
            kw['questype'] = get_docdos_type_id(self.context, data.get('questype'))
        if data.get('l_doctypes'):
            kw['doctype'] = [get_docdos_type_id(self.context, x) for x in data.get('l_doctypes')]
        if data.get('l_dostypes'):
            kw['dostype'] = [get_docdos_type_id(self.context, x) for x in data.get('l_dostypes')]
        if data.get('l_questypes'):
            kw['questype'] = [get_docdos_type_id(self.context, x) for x in data.get('l_questypes')]

        if data.get('start') and data.get('end'):
            kw['dateDoc'] = {'query': [data.get('start'), data.get('end')], 'range': 'minmax'}
        elif data.get('start'):
            kw['dateDoc'] = {'query': data.get('start'), 'range': 'min'}
        elif data.get('end'):
            kw['dateDoc'] = {'query': data.get('end'), 'range': 'max'}

        if data.get('session'):
            kw['session'] = tabellio.config.utils.get_list_of_sessions(data.get('session'))

        if data.get('authors'):
            kw['authorsDoc'] = {'query': data.get('authors').strip().split(),
                                'operator': 'and'}

        if data.get('participants'):
            kw['interveningPersonsDoc'] = {
                    'query': data.get('participants').strip().split(),
                    'operator': 'and'}

        if data.get('polgroups'):
            kw['polgroupsDoc'] = {'query': data.get('polgroups').strip().split(),
                                  'operator': 'and'}
        if data.get('l_polgroups'):
            kw['polgroupsDoc'] = {'query': data.get('l_polgroups'),
                                  'operator': 'and'}

        if data.get('topics'):
            kw['topics'] = {'query': data.get('topics').strip().split(),
                            'operator': 'and'}
        if data.get('l_topics'):
            kw['topics'] = {'query': [possible_topics(self.context).getTermByToken(x).title for x in data.get('l_topics')],
                                  'operator': 'and'}

        if data.get('adopted'):
            kw['adopted'] = data.get('adopted')

        if data.get('decreted'):
            kw['decreted'] = data.get('decreted')

        if not kw and not data.get('text'):
            return []

        if data.get('text'):
            portal = getToolByName(self.context, 'portal_url').getPortalObject()
            if hasattr(portal, 'db'):
                # plaintext search, get document ids from postgresql
                kw['id'] = self.get_ids_from_postgres(data.get('text'))
            else:
                kw['SearchableText'] = data.get('text')

        if data.get('search_type_is_document'):
            kw['portal_type'] = 'tabellio.documents.document'
        elif data.get('search_type_is_adopteddocument'):
            kw['portal_type'] = 'tabellio.documents.document'
        elif data.get('search_type_is_dossier'):
            kw['portal_type'] = 'tabellio.documents.dossier'
        elif data.get('search_type_is_question'):
            kw['portal_type'] = 'tabellio.documents.question'

        cmp_function = cmpfunctions.Cmp().cmp_session
        if data.get('sort_on') == 'type':
            if data.get('search_type_is_document'):
                cmp_function = cmpfunctions.Cmp().cmp_doctype
            elif data.get('search_type_is_adopteddocument'):
                cmp_function = cmpfunctions.Cmp().cmp_doctype
            elif data.get('search_type_is_dossier'):
                cmp_function = cmpfunctions.Cmp().cmp_dostype
            elif data.get('search_type_is_question'):
                cmp_function = cmpfunctions.Cmp().cmp_questype
        elif data.get('sort_on') == 'number':
            cmp_function = cmpfunctions.Cmp().cmp_number
        elif data.get('sort_on') == 'session':
            cmp_function = cmpfunctions.Cmp().cmp_session

        catalog = getToolByName(self.context, 'portal_catalog')
        print 'kw:', kw
        log.info('Performing search (%r) (%r)' % (kw, cmp_function))
        return sorted(catalog(**kw), cmp_function)

    def deputy_search_form(self):
        f = DeputySearchForm(self.context, self.request)
        f.update()
        return f.render()

    def document_pfb_search_form(self):
        f = DocumentPfbSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def question_pfb_search_form(self):
        f = QuestionPfbSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def document_search_form(self):
        f = DocumentSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def document_search_results(self):
        return self.search_results(search_type='document')

    def adopted_document_pfb_search_form(self):
        f = AdoptedDocumentPfbSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def adopted_document_search_results(self):
        return self.search_results(search_type='adopteddocument')

    def dossier_search_form(self):
        f = DossierSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def dossier_search_results(self):
        return self.search_results(search_type='dossier')

    def question_search_form(self):
        f = QuestionSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def question_search_results(self):
        return self.search_results(search_type='question')

    def event_search_form(self):
        f = EventSearchForm(self.context, self.request)
        f.update()
        return f.render()

    def event_search_results(self):
        f = EventSearchForm(self.context, self.request)
        f.update()

        data, errors = f.extractData()
        kw = {}

        if not data.get('search_type_is_event'):
            return None

        if data.get('start') and data.get('end'):
            kw['start'] = {'query': [data.get('start'), data.get('end')], 'range': 'minmax'}
        elif data.get('start'):
            kw['start'] = {'query': data.get('start'), 'range': 'min'}
        elif data.get('end'):
            kw['start'] = {'query': data.get('end'), 'range': 'max'}

        if data.get('text'):
            kw['SearchableText'] = data.get('text')

        kw['sort_on'] = 'start'
        kw['sort_order'] = 'descending'

        kw['portal_type'] = ('tabellio.agenda.event',
                             'tabellio.agenda.comevent',
                             'tabellio.agenda.parlevent')

        catalog = getToolByName(self.context, 'portal_catalog')
        return catalog(**kw)

    def docform_url(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        try:
            return catalog(
                portal_type='Folder',
                object_provides=('tabellio.searchform.form.IFolderWithPfbDocuments',
                                 'tabellio.searchform.form.IFolderWithDocuments'),
                limit=1)[0].getObject().absolute_url()
        except IndexError:
            return '#'

    def deputiesform_url(self):
        catalog = getToolByName(self.context, 'portal_catalog')
        try:
            return catalog(
                portal_type='Folder',
                object_provides=('tabellio.webviews.deputy.IDeputiesAndOthersFolderView',
                                 'tabellio.webviews.deputy.IPfbDeputiesFolderView',
                                 'tabellio.webviews.deputy.IFolderView'),
                limit=1)[0].getObject().absolute_url()
        except IndexError:
            return '#'

    def eventform_url(self):
        return self.portal_url() + '/eventsearch'

    def decrets_years(self):
        now = time.localtime()
        return range(1973, now[0]+1)

    def decrets_year(self):
        now = time.localtime()
        year = int(self.request.form.get('decretyear') or now[0])
        return year

    def decrets(self):
        now = time.localtime()
        year = int(self.request.form.get('decretyear') or now[0])

        start = datetime.datetime(year, 1, 1)
        end = datetime.datetime(year+1, 1, 1)

        catalog = getToolByName(self.context, 'portal_catalog')
        rs = catalog(portal_type='tabellio.documents.document',
                     doctype='DECCCF',
                     dateDoc={'query': (start, end), 'range': 'min:max'},
                     sort_on='dateDoc')
        return rs

    def get_help_links(self):
        try:
            help_dir = getattr(getattr(self.context, 'aide-en-ligne'), 'aide-contextuelle')
        except AttributeError:
            return []
        return help_dir.objectValues()


class Reflex(BrowserView):
    def __call__(self):
        kw = {}
        if self.request.form.get('no'):
            kw['no'] = self.request.form.get('no')
        if self.request.form.get('sess'):
            kw['session'] = self.request.form.get('sess')
        if self.request.form.get('nodoc'):
            kw['nodoc'] = self.request.form.get('nodoc')

        if self.request.form.get('type') == 'document':
            kw['portal_type'] = 'tabellio.documents.document'
        elif self.request.form.get('type') == 'dossier':
            kw['portal_type'] = 'tabellio.documents.dossier'

        catalog = getToolByName(self.context, 'portal_catalog')
        rs = catalog(**kw)
        if not rs:
            return self.request.response.redirect('.')
        return self.request.response.redirect(rs[0].getURL())
